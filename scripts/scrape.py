import scrapy

from scrapy.crawler import CrawlerProcess
from scrapy.dupefilters import RFPDupeFilter

from tnt_backend.models import Article


class TntSpider(scrapy.Spider):
    """
    A spyder which scrapes all the articles from the Teonite blog. It collects article's text, author name,
    title and url, and emits collected data down to the processing pipeline
    """

    name = 'TNT spider'
    start_urls = ['https://teonite.com/blog/']

    def parse(self, response):
        """
        Default entry point to process fetched page. Here it collects article links, emits them to the
        `parse_article` method, and "goes" to the next page with articles
        """

        for article_url in response.css('h2.post-title > a ::attr("href")').extract():
            yield response.follow(article_url, callback=self.parse_article)

        next_page = response.css('a.older-posts::attr("href")').extract_first()
        yield response.follow(next_page, callback=self.parse)

    def parse_article(self, response):
        """
        A method to get data from a single article page
        """

        content = response.xpath(".//section[@class='post-content']/descendant::text()").extract()

        yield {
            'title': response.css('.post-title ::text').extract_first(),
            'author': response.css('.author-content > h4 ::text').extract_first(),
            'content': ' '.join(content),
            'url': response.request.url,
        }


class SaveToDB(object):
    """
    A deadly simple pipeline item, the main goal of which is just to save collected data
    """

    def process_item(self, item, spider):
        Article.add(item)


class SavedArticlesFilter(RFPDupeFilter):
    """
    A duplication filter which checks DB againts the next request the spader is going to scrape.
    It also has to keep tracking of the pagination url's, that's why we call `super().request_seen(request)`
    """

    def request_seen(self, request):
        return Article.is_already_saved(request.url) or super().request_seen(request)


def run():
    """
    This method is going to be called by the 'runscript' Django expension. Everything we need to do have to
    be in this method
    """

    process = CrawlerProcess({
        'CONCURRENT_REQUESTS_PER_DOMAIN':16,
        'DUPEFILTER_CLASS': 'scripts.scrape.SavedArticlesFilter',
        'ITEM_PIPELINES': {
            'scripts.scrape.SaveToDB': 10,
        },
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
    })

    process.crawl(TntSpider)
    process.start() # the script will block here until the crawling is finished
