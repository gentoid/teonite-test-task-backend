import datetime
import re

from collections import Counter
from django.db import models
from django.utils import timezone
from functools import reduce
from unidecode import unidecode


class Word(models.Model):
    value = models.CharField(max_length=100, unique=True)

    @classmethod
    def parse_and_add(cls, content):
        """
        A method, which gets all the words from the passed string, adds them to DB, and returns
        a list of collected words. It will return all the found words as is, with all repetitions
        """
        words = re.findall(r'\w+', content.lower())

        for word in set(words):
            cls.objects.get_or_create(value=word)

        return words


class Author(models.Model):
    name = models.CharField(max_length=100, unique=True)
    alias = models.CharField(max_length=100, unique=True)

    @classmethod
    def alphabetically(cls):
        """
        This method returns a list of all the Authors from DB ordered by 'alias' alphabetically
        """
        return cls.objects.order_by('alias')

    @classmethod
    def add(cls, author_name):
        """
        A method which adds new Author. It prepares 'alias' on its own from the passed author name
        """
        alias = (unidecode(author_name)
            .lower()
            .replace(' ', ''))

        return cls.objects.get_or_create(alias=alias, defaults={'name': author_name})


class Article(models.Model):
    url = models.CharField(max_length=255, unique=True)
    title = models.CharField(max_length=255)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)

    @classmethod
    def is_already_saved(cls, url):
        """
        This method checks if there's an Article with given URL already
        """
        return len(cls.objects.filter(url=url)) > 0

    @classmethod
    def add(cls, scraped_item):
        """
        This method adds all the required information to DB based on the passed 'scraped_item'.
        It's assumed, that the Scraper's Pipeline in order to save scraped data will use this method
        only. But please be warned, this method will raise an exception if you'll try to save same
        article more than once - it's implied that before saving one should check if there's such
        an Article there with help of 'is_already_saved'
        """
        (author, _) = Author.add(scraped_item['author'])
        article = cls.objects.create( # TODO: add validations to check the data
            url=scraped_item['url'],
            author=author,
            title=scraped_item['title'],
        )

        WordsInArticle.add_content_to_article(article, scraped_item['content'])


class WordsInArticle(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    counter = models.IntegerField(default=0)

    class Meta:
        # It's better to restrict such cases, otherwise it may bring surprises in the future
        unique_together = (('article', 'word'),)

    @classmethod
    def top_words(cls, author_alias=None):
        """
        This methods prepares top 10 words. If there was Author's alias provided, then it'll
        will look up for its articles only, otherwise it'll return statistics for all the articles
        in database
        """

        # TODO: IF possible, it's better to improve this, otherwise it looks ugly.
        #       Probably, it's possible to use F and/or Q here
        raw_sql = (
            'SELECT word_id AS id, value, count FROM '
            '(SELECT word_id, SUM(counter) AS count FROM tnt_backend_wordsinarticle '
        )

        if author_alias is not None:
            raw_sql = raw_sql + (
                'WHERE article_id IN (SELECT id FROM tnt_backend_article WHERE author_id IN '
                '(SELECT id FROM tnt_backend_author WHERE alias = %s)) '
            )

        raw_sql = raw_sql + (
            'GROUP BY word_id ORDER BY count DESC LIMIT 10) AS t1 '
            'LEFT JOIN tnt_backend_word AS t2 ON t1.word_id = t2.id ORDER BY count DESC'
        )

        return cls.objects.raw(raw_sql, [author_alias])

    @classmethod
    def add_content_to_article(cls, article, content):
        """
        This method creates instanses of self for every unique combination of Article and Word
        from 'content'. It uses 'Word.parse_and_add' method to get words from the 'content'
        """
        words_with_counts = Counter(Word.parse_and_add(content))

        for word in words_with_counts:
            cls.objects.create(
                article=article,
                word=Word.objects.get(value=word),
                counter=words_with_counts[word],
            )
