from django.urls import path

from . import views

app_name = 'tnt_backend'
urlpatterns = [
    path('authors/', views.Authors.as_view(), name='authors_list'),
    path('stats/', views.Stats.as_view(), name='general_stats'),
    path('stats/<str:author_alias>/', views.Stats.as_view(), name='author_stats'),
]
