from django.test import TestCase
from django.urls import reverse

from .prepare_article import PrepareArticleMixin

class GeneralStatsResource(PrepareArticleMixin, TestCase):

    def test_empty_json_object_if_theres_no_data(self):
        """
        It should return an empty JSON object, if there's, no data in the database
        """
        response = self.client.get(reverse('tnt_backend:general_stats'))
        self.assertEqual(response.content, b'{}')

    def test_some_json_object_if_theres_data_there(self):
        """
        It should return some JSON object, if there's some data there
        """
        self.prepare_article('Szczecin', 'Some Author', 'Hello hello')

        response = self.client.get(reverse('tnt_backend:general_stats'))
        self.assertEqual(response.content, b'{"hello":2}')

    def test_it_orders_data_by_counters(self):
        """
        For the predictibility purposes we'd like to have all the data ordered by counters in the reversed order
        """
        self.prepare_article('Szczecin', 'One Author', 'Hello world! Hello all! Hello world')

        response = self.client.get(reverse('tnt_backend:general_stats'))
        self.assertEqual(response.content, b'{"hello":3,"world":2,"all":1}')
