from django.test import TestCase
from django.urls import reverse

from .prepare_article import PrepareArticleMixin

class AuthorStatsResource(PrepareArticleMixin, TestCase):

    def test_it_shows_stats_for_the_requested_author_only(self):
        """
        It should return the data related to the requested author only
        """
        self.prepare_article('Szczecin', 'Some Author', 'Hello hello hello')
        self.prepare_article('Stargard', 'Another Author', 'hi there hello world')

        response = self.client.get(reverse('tnt_backend:author_stats', args=('someauthor',)))
        self.assertEqual(response.content, b'{"hello":3}')

    def test_it_sums_stats_for_all_the_articles_of_the_requested_author(self):
        """
        It have to sum all the words across all the author's articles, and show them sorted
        """
        self.prepare_article('Szczecin', 'Some Author', 'Hello Szczecin hello Szczecin hello Garden')
        self.prepare_article('Police', 'Some Author', 'hello Szczecin floating garden')

        response = self.client.get(reverse('tnt_backend:author_stats', args=('someauthor',)))
        self.assertEqual(response.content, b'{"hello":4,"szczecin":3,"garden":2,"floating":1}')
