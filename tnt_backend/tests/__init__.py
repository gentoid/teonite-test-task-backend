from .general_stats import GeneralStatsResource
from .author_stats import AuthorStatsResource
from .authors_list import AuthorsListResource
