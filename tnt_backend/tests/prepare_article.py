import uuid

from tnt_backend.models import Article

class PrepareArticleMixin:

    def prepare_article(self, article, author_name, content):
        Article.add({
            'url': str(uuid.uuid4()),
            'author': author_name,
            'content': content,
            'title': 'Some Title',
        })
