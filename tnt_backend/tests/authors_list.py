from django.test import TestCase
from django.urls import reverse

from .prepare_article import PrepareArticleMixin

class AuthorsListResource(PrepareArticleMixin, TestCase):

    def test_returns_empty_object_if_theres_no_authors(self):
        """
        It should return empty JSON object if there's no added author yet
        """
        response = self.client.get(reverse('tnt_backend:authors_list'))
        self.assertEqual(response.content, b'{}')

    def test_returns_authors_ordered_alphabetically(self):
        """
        It should return authors in alphabetical order
        """
        self.prepare_article('Szczecin', 'Some Author', '')
        self.prepare_article('Police', 'Another Author', '')
        self.prepare_article('Goleniów', 'One more cool Author', '')

        response = self.client.get(reverse('tnt_backend:authors_list'))
        expect = b'{"anotherauthor":"Another Author","onemorecoolauthor":"One more cool Author","someauthor":"Some Author"}'
        self.assertEqual(response.content, expect)
