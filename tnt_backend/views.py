from django.shortcuts import render
from functools import reduce
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Article, Author, Word, WordsInArticle


class Authors(APIView):
    """
    A view that returns all the scraped authors in alphabetical order
    """
    renderer_classes = (JSONRenderer, )

    def get(self, request, format=None):
        authors = {row.alias: row.name for row in Author.alphabetically()}
        return Response(authors)


class Stats(APIView):
    """
    A view that returns the count of all the Articles
    """
    renderer_classes = (JSONRenderer, )

    def get(self, request, author_alias=None, format=None):
        words_with_counts = WordsInArticle.top_words(author_alias=author_alias)
        stats = {row.value: row.count for row in words_with_counts}
        return Response(stats)
