from django.apps import AppConfig


class TntBackendConfig(AppConfig):
    name = 'tnt_backend'
